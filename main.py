#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys


def main():
    print('Hello, world')
    print('This is {}'.format(sys.argv[0]))

if __name__ == "__main__":
    main()

